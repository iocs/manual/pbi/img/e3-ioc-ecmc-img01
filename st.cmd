require essioc
iocshLoad "${essioc_DIR}/common_config.iocsh"

epicsEnvSet IOC PBI-IMG01:Ctrl-ECAT-100

require ecmccfg 8.0.0
iocshLoad "${ecmccfg_DIR}/startup.cmd" "ECMC_VER=8.0.2, NAMING=ESSnaming"
ecmcConfigOrDie "Cfg.SetDiagAxisEnable(0)"

iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EK1100
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL3255
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL3255
iocshLoad "${E3_CMD_TOP}/hw/addSlaveLocal.cmd" "HW_DESC=EL7342, SUBST_FILE=${E3_CMD_TOP}/hw/ecmcEL7342.substitutions"
iocshLoad "${E3_CMD_TOP}/hw/addSlaveLocal.cmd" "HW_DESC=EL7342, SUBST_FILE=${E3_CMD_TOP}/hw/ecmcEL7342.substitutions"
iocshLoad "${E3_CMD_TOP}/hw/addSlaveLocal.cmd" "HW_DESC=EL7342, SUBST_FILE=${E3_CMD_TOP}/hw/ecmcEL7342.substitutions"
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL1808
iocshLoad "${ecmccfg_DIR}/configureSlave.cmd" "HW_DESC=EL7041-0052, CONFIG=-Motor-Phytron-VSS-57.200.2.5"
iocshLoad "${ecmccfg_DIR}/configureSlave.cmd" "HW_DESC=EL7041-0052, CONFIG=-Motor-Phytron-VSS-57.200.2.5"
iocshLoad "${ecmccfg_DIR}/configureSlave.cmd" "HW_DESC=EL7041-0052, CONFIG=-Motor-Phytron-VSS-57.200.2.5"
iocshLoad "${ecmccfg_DIR}/addSlave.cmd" HW_DESC=EL3314

#iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis1.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis2.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis3.ax"
#iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis4.ax"
#iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis5.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis6.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis7.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis8.ax"
iocshLoad "${ecmccfg_DIR}/configureAxis.cmd" "CONFIG=${E3_CMD_TOP}/cfg/axis9.ax"

iocshLoad "${ecmccfg_DIR}/applyConfig.cmd"
iocshLoad "${ecmccfg_DIR}/setAppMode.cmd"

dbLoadRecords "${E3_CMD_TOP}/db/archiver.db" "P=${SM_PREFIX}"
dbLoadRecords "${E3_CMD_TOP}/db/alarms.db" "P=${SM_PREFIX}, MASTER_ID=${ECMC_EC_MASTER_ID}"
