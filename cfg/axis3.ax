# ECMC_HOME_DEC has no effect at all but we need to define it otherwise
# the IOC will not load the PVs correctly. Remove me when it is fixed.
# https://github.com/paulscherrerinstitute/ecmccfg/pull/197
epicsEnvSet ECMC_HOME_DEC ""

# General
epicsEnvSet ECMC_AXIS_NO 3
epicsEnvSet ECMC_MOTOR_NAME Axis${ECMC_AXIS_NO}
epicsEnvSet ECMC_DESC "DumpL IMG screen ${ECMC_AXIS_NO}"
epicsEnvSet ECMC_EGU mm
epicsEnvSet ECMC_PREC 3
epicsEnvSet ECMC_AXISCONFIG ""
epicsEnvSet ECMC_EC_AXIS_HEALTH ""

# Encoder
epicsEnvSet ECMC_ENC_SCALE_NUM 0.15875
epicsEnvSet ECMC_ENC_SCALE_DENOM 12800
epicsEnvSet ECMC_ENC_TYPE 0
epicsEnvSet ECMC_ENC_BITS 16
epicsEnvSet ECMC_ENC_ABS_BITS 0
epicsEnvSet ECMC_ENC_ABS_OFFSET 0
epicsEnvSet ECMC_EC_ENC_ACTPOS ec0.s9.positionActual01

# Drive
epicsEnvSet ECMC_DRV_SCALE_NUM 1.5875
epicsEnvSet ECMC_DRV_SCALE_DENOM 32767
epicsEnvSet ECMC_EC_DRV_CONTROL ec0.s9.driveControl01.0
epicsEnvSet ECMC_EC_DRV_STATUS ec0.s9.driveStatus01.1
epicsEnvSet ECMC_EC_DRV_VELOCITY ec0.s9.velocitySetpoint01
epicsEnvSet ECMC_EC_DRV_REDUCE_TORQUE ec0.s9.driveControl01.2
epicsEnvSet ECMC_EC_DRV_BRAKE ""
epicsEnvSet ECMC_DRV_BRAKE_OPEN_DLY_TIME 0
epicsEnvSet ECMC_DRV_BRAKE_CLOSE_AHEAD_TIME 0
epicsEnvSet ECMC_EC_DRV_RESET ec0.s9.driveControl01.1
epicsEnvSet ECMC_EC_DRV_ALARM_0 ec0.s9.driveStatus01.3
epicsEnvSet ECMC_EC_DRV_ALARM_1 ""
epicsEnvSet ECMC_EC_DRV_ALARM_2 ec0.s9.driveStatus01.13
epicsEnvSet ECMC_EC_DRV_WARNING ec0.s9.driveStatus01.2

# Trajectory
epicsEnvSet ECMC_VELO 0.5
epicsEnvSet ECMC_JOG_VEL ${ECMC_VELO}
epicsEnvSet ECMC_ACCS_EGU_PER_S2 ${ECMC_VELO}
epicsEnvSet ECMC_JAR ${ECMC_ACCS_EGU_PER_S2}
epicsEnvSet ECMC_EMERG_DECEL ${ECMC_ACCS_EGU_PER_S2}

# Homing
epicsEnvSet ECMC_HOME_PROC 2
epicsEnvSet ECMC_HOME_POS 440
epicsEnvSet ECMC_HOME_VEL_TO ${ECMC_VELO}
epicsEnvSet ECMC_HOME_VEL_FRM ${ECMC_HOME_VEL_TO}
epicsEnvSet ECMC_HOME_ACC ${ECMC_ACCS_EGU_PER_S2}

# Controller
epicsEnvSet ECMC_CNTRL_KP 15.0
epicsEnvSet ECMC_CNTRL_KI 0.02
epicsEnvSet ECMC_CNTRL_KD 0.0
epicsEnvSet ECMC_CNTRL_KFF 1.0

# Switches
epicsEnvSet ECMC_EC_MON_LOWLIM ec0.s6.binaryInput08.0
epicsEnvSet ECMC_EC_MON_HIGHLIM ec0.s6.binaryInput07.0
epicsEnvSet ECMC_EC_MON_HOME_SWITCH ec0.s0.ZERO.0
epicsEnvSet ECMC_EC_MON_EXT_INTERLOCK ec0.s0.ONE.0

# Softlimits
epicsEnvSet ECMC_SOFT_LOW_LIM -1
epicsEnvSet ECMC_SOFT_HIGH_LIM 441
epicsEnvSet ECMC_DXLM_ENABLE 1

# Position lag
epicsEnvSet ECMC_MON_LAG_MON_TOL 0
epicsEnvSet ECMC_MON_LAG_MON_TIME 0
epicsEnvSet ECMC_MON_LAG_MON_ENA 0

# At target
epicsEnvSet ECMC_MON_AT_TARGET_TOL 0.002
epicsEnvSet ECMC_MON_AT_TARGET_TIME 0  # tune me
epicsEnvSet ECMC_MON_AT_TARGET_ENA 1

# Velocity
epicsEnvSet ECMC_MON_VELO_MAX 0
epicsEnvSet ECMC_MON_VELO_MAX_TRAJ_TIME 1
epicsEnvSet ECMC_MON_VELO_MAX_DRV_TIME 0
epicsEnvSet ECMC_MON_VELO_MAX_ENA 0
